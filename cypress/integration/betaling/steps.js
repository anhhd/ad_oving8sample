import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

//Scenario 1
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').clear().type('Per Hansen');
    cy.get('#address').clear().type('Persenveien 1');
    cy.get('#postCode').clear().type('0585');
    cy.get('#city').clear().type('Oslo');
    cy.get('#creditCardNo').clear().type('1111111111111111');

});

And(/^trykker på Fullfør kjøp$/, function () {
    cy.get('input[type=submit]').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('contain', 'Din ordre er registrert.');
});

//Scenario 2
//Negative assertion
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear();
    cy.get('#address').clear().type('bb');
    cy.get('#postCode').clear().type('postnr');
    cy.get('#city').clear().type('Oslo');
    cy.get('#creditCardNo').clear().type('1111').blur(); //Plotter en feil med vilje

});

//Klarer ikke å matche feilmeldinger
Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.get('#fullNameError').should('contain', 'Feltet må ha en verdi'); //Ok
    cy.get('#creditCardNoError').should('contain', 'Kredittkortnummeret må bestå av 16 siffer'); //Ikke ok. "AssertionError: Timed out retrying after 4000ms: expected '<span#creditCardNoError.formError>' to contain 'Kredittkortnummeret må bestå av 16 siffer'  "
});

